export interface Config {
    discordAuthToken: string;
    giphyToken: string;
    persona?: string;
    guildId: string;
}