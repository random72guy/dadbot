export interface Riddle { q: string; a: string; };

export const riddles: Riddle[] = [
    {
        q: 'Why did the barber win the race?',
        a: 'cut',
    },
    {
        q: 'What do you call a group of killer whales playing instruments?',
        a: 'orca',
    },
    {
        q: 'I don’t play soccer because I enjoy the sport. I’m just doing it ___ _____.',
        a: 'for kicks',
    },
    {
        q: `I couldn't get a reservation at the library because they were completely...`,
        a: 'booked',
    },
    {
        q: 'A cannibal is someone who is ___ __ with people.',
        a: 'fed up',
    },
    {
        q: 'Why did the coffee file a police report?',
        a: 'mugged',
    },
    {
        q: 'What did the mountain climber name his son?',
        a: 'cliff',
    },
    {
        q: 'I never trust people with graph paper because...',
        a: 'plotting something',
    },
    {
        q: 'What do you give a sick lemon?',
        a: 'lemon',
    },
    {
        q: 'How do the trees get on the internet?',
        a: 'log ',
    },
    {
        q: `Recent survey revealed 6 out of 7 dwarf's aren't`,
        a: 'happy',
    },
    {
        q: 'What cheese can never be yours?',
        a: 'nacho',
    },
    {
        q: 'A book just fell on my head. I only have my _____ to blame.',
        a: 'shelf',
    },
    {
        q: `What's brown and sticky?`,
        a: 'stick',
    },
    {
        q: 'Why did the kid throw the clock out the window?',
        a: 'time fl',
    },
    {
        q: `I hate perforated lines, they're ________.`,
        a: 'tearable',
    },
    {
        q: `Why don't sharks eat clowns?`,
        a: 'taste funny',
    },
    {
        q: 'How do you organize a space party?',
        a: 'plan',
    },
    {
        q: 'You will never guess what Elsa did to the balloon.',
        a: 'let it go',
    },
    {
        q: 'Why was ten scared of seven?',
        a: 'seven ate nine',
    },
];
