# Dadbot

Run with `npm start` once you've added a `config.ts` file matching the `config.interface.ts` interface.

Add bot to server with this URL: https://discordapp.com/oauth2/authorize?client_id=653398783459065886&scope=bot&permissions=8

## Hosting

[pm2](https://github.com/Unitech/pm2) can be used to keep Dadbot running on a server. After installing on your server, use `pm2 start npm -- start`.

You can have it start at startup with pm2 startup.

You can restart with `pm2 restart <app-name>`. `app-name` can be found using `pm2 list`.

## Helpful Links

* [Discord Developer Portal](https://discordapp.com/developers)
* [Discord.js library](https://discord.js.org/#/docs/main/stable/class/Client)
* [ICanHazDadjoke API](https://icanhazdadjoke.com/api#search-for-dad-jokes)
* [Giphy](https://giphy.com/)
* [pm2 quickstart guide](https://pm2.keymetrics.io/docs/usage/quick-start/)
