import { TextChannel, Client, User } from "discord.js";
import * as _ from 'lodash';
import { passphraseRitual } from "./main";

export class RitualCompletion {

    private ritualSet = new RitualSteps();
    private client: Client;
    private mainChannel: TextChannel;

    constructor(
        client: Client,
        mainChannel: TextChannel
    ) {
        this.client = client;
        this.mainChannel = mainChannel;
    }

    public markComplete(ritualName: keyof RitualSteps) {
        this.ritualSet[ritualName] = true;

        // Trigger passphrase finale if others are complete.
        if (_.countBy(this.ritualSet).false == 1)
            passphraseRitual();

        // // Trigger completion if all complete
        // if (this.allAreComplete())
        //     this.completeSummoningRitual();
    }

    public isComplete(ritualName: keyof RitualSteps) {
        return this.ritualSet[ritualName];
    }

    public getCompletionPercent(): number {
        const count: number = _.countBy(this.ritualSet).true || 0;
        return _.round( count / _.size(this.ritualSet))  * 100;
    }

    public allAreComplete(): boolean {
        return _.countBy(this.ritualSet).true == _.size(this.ritualSet);
    }

    /**
    * Ask me to complete summoning Dadbot. I'll have to manually change the name and ID.
    */
    public completeSummoningRitual(): void {

        // Find me by my unique ID.
        const nan: User = _.find(this.client.users.array(), user => user.id == '206930348972572673'
        );

        this.mainChannel.send('Complete the ritual, *now*!!', {
            reply: nan
        });

    }
}

class RitualSteps {
    childSacrifice: boolean =false; // Place kid emoji into a cauldron.
    riddles: boolean = false;       // Answer the punchline to dad jokes or riddles.
    // banishment: boolean = false;    // Certain people must leave and come back.
    nicknames: boolean = false;     // A number of people must change their names.
    passphrase: boolean = false;    // The phrase "Bring back Dadbot!" must be said multiple times. (Provide hint after first time.)
}
