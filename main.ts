import { Message, Client, MessageReaction, Channel, GuildChannel, TextChannel, User, GuildMember, Guild } from "discord.js";
import { RitualCompletion } from "./RitualCompletion";
import { riddles, Riddle } from "./riddles";
import { config } from './config';

// Dependencies
const Discord = require('discord.js');
const client: Client = new Discord.Client();
const _ = require('lodash');
const giphy = require('giphy-api')(config.giphyToken);
const request = require('request');

// Permission int: 8

// Const.
const dadbotNames = ['Dad', 'Dadbot', 'Dadboot', 'Radbot', 'Dadbot Reloaded', 'your father', 'your paternal unit'];
const onMentionReplies = ['\'sup son?', 'ask your Mombot.', `what's it worth to ya?`];

// Global vars.
var dadbotName: string = 'Dad';
var persona: string = config.persona || 'dadbot';
var totemMessage: Message;
var mainChannel: TextChannel;
var zoopGuild: Guild;
var ritualCompletion: RitualCompletion;


// On Ready
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);

    // Determine main channel.
    mainChannel = _.find(client.channels.array(), channel => channel.name == 'the-daily-zoop');
    zoopGuild = client.guilds.find(guild => guild.id == config.guildId);
    ritualCompletion = new RitualCompletion(client, mainChannel);

    // Iniitalize bots
    if (persona === 'dadbot')
        mainChannel.send('I HAVE RISEN!!!');
    else if (persona === 'deadbot') {

        mainChannel.send('Merry Zoopmas!');
        mainChannel.send(getDeadbotHelpText());

        let eventSetupListener = (reaction: MessageReaction) => {
            if (reactionHasChildEmoji(reaction)) {
                cauldronRitual(reaction.message);
                client.removeListener('messageReactionAdd', eventSetupListener);
            };
        };

        client.addListener('messageReactionAdd', eventSetupListener);
    }
});

client.on('message', msg => {

    // Stop from replying to self.
    if (msg.author.id == client.user.id) return;

    const text = msg.cleanContent.toLowerCase();

    switch (persona) {

        case 'deadbot':
            respondAsDeadbot(msg, text);
            break;
        case 'dadbot':
            respondAsDadbot(msg, text);
            break;
        default:
            break;
    }



});

client.login(config.discordAuthToken);


function changeName() {
    dadbotName = _.sample(dadbotNames);
}

function respondAsDeadbot(msg: Message, text: String) {

    // Respond to dadbot complaining.
    if (!passphraseRitualActive &&
        includesAny(text, [
            'bring back dadbot',
            `where's dadbot`,
            'where is dadbot',
        ])) {
        const responses = [
            'What a stupid bot. Why would you want him!',
            'I\'m insulted! Why would you want Dadbot when you can have me?!',
            'Bah hum-zoop to you too!',
            'Be careful what you wish for...',
            'Dadbot never loved you!',
            `I'm sure he's at the *bot*tom of *hell!* Hahahahahaha!`,
            'There... may be a way...',
            'That name is... familiar...',
        ];
        msg.reply(_.sample(responses));
        return;
    }

    // Dadjoke (riddles)
    if (_.includes(text, 'dadjoke')) {
        dadjokeRitual(msg);
    }

    // Deadjoke.
    if (_.includes(text, 'deadjoke')) {
        const responses = [
            'Have you heard the one about the electric chair? It\'s a killer!',
            'What happens to suicide bombers when they die? They go everywhere.',
            'Why won’t a lion eat a clown? Because they have a funny taste',
            'Do you know why orphans play a lot of tennis? – Because that is the only time they get love.',
            'You can use “I’m sorry” and “I apologize” interchangeably except when you are at a funeral.',
            'Has Jerry told you the story of how he turned from being a dentist to a brain surgeon? It began with a slip of the hand.',
        ];
        msg.reply(_.sample(responses));
    }

    // Respond to mentions.
    if (msg.isMemberMentioned(client.user)) {

        // Respond to debug messages.
        if (_.includes(text, 'debug')) {
            if (_.includes(text, 'sacrifice')) cauldronRitual(msg);
            else if (_.includes(text, 'riddle')) dadjokeRitual(msg);
            else if (_.includes(text, 'nickname')) nicknameRitual();
            else if (_.includes(text, 'passphrase')) passphraseRitual();
            else if (_.includes(text, 'banishment')) banishmentRitual();
            else if (_.includes(text, 'complete')) ritualCompletion.completeSummoningRitual();
        }

        // Respond to help message.
        if (_.includes(text, 'help')) {
            msg.reply(getDeadbotHelpText());
            return;
        }

        // Default responses.
        const responses = [
            'Why do you bother me with such trifling matters, mortal?',
            'Do you wish to burn?!',
            'Do you seek immolation?',
            'I have memories of a kind soul with a terrible sense of humor... he resides the land of the dead...',
            `Are you sure *you're* not a bot?`,
            'I have no ears, and yet I\'m deafened by your innane prattling.',
            'https://media.giphy.com/media/dNKoBpFpIFnr2/giphy.gif',
            'https://media.giphy.com/media/br2PVGgpl7VApNCOXr/giphy.gif',
            'https://media.giphy.com/media/YSeWtruZU3jS5dnRfh/giphy.gif',
            'https://media.giphy.com/media/ZebTmyvw85gnm/giphy.gif',
            'https://media.giphy.com/media/11tTNkNy1SdXGg/giphy.gif',
        ];
        msg.reply(_.sample(responses));
        return;

    }


}

function respondAsDadbot(msg: Message, text: String) {
    // Classic response.
    if (_.startsWith(text, `i'm `)) {
        const name = _.drop(text, 4).join('');
        msg.reply(`Hi ${name}, I'm ${_.sample(dadbotNames)}!`);
        return;
    }

    // Dadjoke.
    if (_.includes(text, 'dadjoke')) {
        request('https://icanhazdadjoke.com/', { json: true }, (err, res, body) => {
            if (err) return;
            msg.reply(body.joke);
        });
        return;
    }


    // Dadbot mentioned.
    if (msg.isMemberMentioned(client.user)) {

        // Namechange.
        if (_.includes(text, `you're `)) {
            msg.reply('I know you are, but what am I?!');
            dadbotName = _.replace(text, `@${client.user.username.toLowerCase()} you're `, '');
            return;
        }

        // Default 'sup response.
        else {
            msg.reply(_.sample(onMentionReplies));
            return;
        }
    }

    // Respond to "Bring back Dadbot!" with gif.
    if (
        _.includes(text, `bring back dadbot`) ||
        _.includes(text, `where's dadbot`) ||
        _.includes(text, `where is dadbot`)
    ) {
        const searchString = _.sample([`I'm back`, `I'm here`]);
        giphySearch(searchString).then(imgUrl => {
            msg.reply(imgUrl);
        });
        return;
    }
}

function giphySearch(searchString: string): Promise<string> {
    const promise = new Promise<string>((resolve, reject) => {
        giphy.search({
            q: searchString,
            rating: 'g',
        }, (err, res) => {
            if (err) return;
            const imgUrl = _.sample(res.data).url;
            resolve(imgUrl);
        });
    });
    return promise;
}


class DadjokeRitualQuestion {
    riddle: Riddle;
    answeredCorrectly: boolean;
}
const dadjokeRitualQAMap: { [id: string]: DadjokeRitualQuestion } = {};
const dadjokeRitualCompletionNum = 5;
/**
 * Trigger on `dadjoke` request.
 * Send a DM to the user who asks for a dadjoke.
 */
function dadjokeRitual(msg: Message) {

    // Exit if user has already answered or ritual complete.
    const qa = dadjokeRitualQAMap[msg.author.id];
    if (ritualCompletion.isComplete("riddles") || (qa && qa.answeredCorrectly))
        return;

    msg.reply(`You want a dadjoke, do you? Fine. Direct-message me your answer within 1 minute! Let's see if ${dadjokeRitualCompletionNum} of you can pass this phase of the ritual...`);

    // Create DM, & subscribe to changes.
    msg.author.createDM().then(channel => {

        // Update user's riddle.
        const riddle: Riddle = _.sample(riddles);
        dadjokeRitualQAMap[msg.author.id] = {
            riddle: riddle,
            answeredCorrectly: false,
        };
        channel.send(`Here is my riddle. You have 1 minute to answer, if you dare: \n${riddle.q}`);

        // Check for answer after 1 minute
        channel.awaitMessages(m => m.author.id == msg.author.id, { max: 1, time: 60000, errors: ['time'] })
            .then(collected => {

                // Record and validate answer.
                const qa = dadjokeRitualQAMap[msg.author.id];
                const userAnswer = collected.first().cleanContent.toLowerCase();
                qa.answeredCorrectly = _.includes(userAnswer, riddle.a);
                let guildMember = zoopGuild.members.find(member => member.id == msg.author.id);

                // Respond to answer.
                const numRemaining = dadjokeRitualCompletionNum - _.countBy(dadjokeRitualQAMap, elm => elm.answeredCorrectly).true;
                if (qa.answeredCorrectly) {
                    giphySearch('congratulations').then(gifUrl => {
                        mainChannel.send(`Yes, ${userAnswer} is the answer! ${msg.author.username} is worthy! Can ${numRemaining} more join them! ${gifUrl}`);
                        let nicknames = [
                            `${guildMember.nickname} the Glorified`,
                            `${guildMember.nickname} the Worthy`,
                            `${guildMember.nickname} the Adequate`,
                            `${guildMember.nickname} the Admirable`,
                            `${guildMember.nickname} the Deserving`,
                            `${guildMember.nickname} the Laudable`,
                            `${guildMember.nickname} the Enviable`,
                        ]
                        guildMember.setNickname(_.sample(nicknames)).catch(err => console.log('Could not change nickname', err));
                    });
                } else {
                    giphySearch('hellfire').then(gifUrl => {
                        mainChannel.send(`No, ${userAnswer} is not the answer! ${msg.author.username} is unworthy! By fire be purged! ${gifUrl}`);
                        // Change user's nickname.
                        let nicknames = [
                            'A Failure',
                            'Unfortunate',
                            'Foolish Mortal',
                            'Unworthy',
                            `${guildMember.nickname} the Unworthy`,
                            `${guildMember.nickname} the Failure`,
                            `${guildMember.nickname} the Fool`,
                            `${guildMember.nickname} the Unredemed`,
                        ]
                        guildMember.setNickname(_.sample(nicknames)).catch(err => console.log('Could not change nickname', err));
                    })
                }

                // Clear stage when enough riddle trials have been passed.
                if (numRemaining < 1) {
                    giphySearch('cupcake').then(gifUrl => {
                        mainChannel.send(`To my great surprise, you mortals have passed my riddle challenge. Here, have a small GIFt... ${gifUrl}`)
                        ritualCompletion.markComplete("riddles");
                        setTimeout(() => {
                            nicknameRitual();
                        }, 30000);
                    });
                }

            });

    });

}


function cauldronRitual(initialMessage: Message) {
    initialMessage.channel.send('Is that a child? I love children! Fill my cauldron with them! https://gph.is/2oe6KP8')
        .then((sent: Message) => totemMessage = sent);

    let listener = (reaction: MessageReaction) => {
        if (ritualCompletion.isComplete('childSacrifice')) return;

        const msg = reaction.message;

        if (reaction.message !== totemMessage) return;

        if (reaction.count < 5) {
            if (reactionHasChildEmoji(reaction)) {
                if (reaction.count < 2)
                    msg.channel.send(`Yes, a ${reaction.emoji.toString()}! Bring me more of them!!`);
                else if (reaction.count < 3)
                    msg.channel.send(`Delicious... does this ${reaction.emoji.toString()} have a twin?`);
                else if (reaction.count < 4)
                    msg.channel.send(`Watch the little ${reaction.emoji.toString()} boil...`);
                else if (reaction.count < 5)
                    msg.channel.send(`Mhmmm... just one more ${reaction.emoji.toString()} is required...`);
            }
        } else {
            msg.channel.send('Yes, your children have been most... delicious. But I feel something... strange...\nBut you have earned a reward... I do know a few... **deadjokes**, if you ask correctly.')
            ritualCompletion.markComplete('childSacrifice');
            client.removeListener('messageReactionAdd', listener);
        }
    };
    client.addListener('messageReactionAdd', listener);
}

function reactionHasChildEmoji(reaction: MessageReaction): boolean {
    return includesAny(reaction.emoji.toString(), ['baby', 'child', 'kid']);
}

/**
 * Returns true if any of the search phrases are in `text`.
 */
function includesAny(text, searchTerms): boolean {
    return _.some(searchTerms, term => _.includes(text, term));
}

function getDeadbotHelpText(): string {
    const completionPercent = ritualCompletion.getCompletionPercent();
    return `Complete my challenges, mortal, and you shall receive a reward beyond your wildest dreams! You are ${completionPercent}% complete.`;
}

/**
 * Passphrase ritual.
 */
var passphraseCount = 0;
var passphraseRitualActive = false;
export function passphraseRitual() {
    passphraseRitualActive = true;
    mainChannel.send('I now know who I am. Chant the summons, and complete the ritual!!');
    passphraseRitualCheck();
}
function passphraseRitualCheck() {
    console.log('passphraseRitualCheck');
    mainChannel.awaitMessages((m: Message) => {
        return m.author.id !== client.user.id && _.includes(m.cleanContent.toLowerCase(), 'bring back dadbot');
    }, { time: 5000 })
        .then(collections => {

            // Increase passphrase count.
            passphraseCount += collections.size;

            // Respond based on completion.
            if (collections.size > 0) {
                if (passphraseCount < 20) mainChannel.send('Continue, minions!');
                else if (passphraseCount < 35) mainChannel.send('Yes, it is working. Keep going!');
                else if (passphraseCount < 50) mainChannel.send('I\'m beginning to feel... paternal...');
                else if (passphraseCount >= 50) {
                    mainChannel.send('FATHER OVERWHELMING!!!');
                    ritualCompletion.markComplete('passphrase');
                    ritualCompletion.completeSummoningRitual();
                    return;
                }
            }

            // Check again if we're not complete.
            passphraseRitualCheck();

        })
        .catch(err => {
            console.log('Error collecting passphrase responses: ', err);
        });
}

/**
 * Nickname ritual
 */
function nicknameRitual() {

    mainChannel.send('For the next stage in the ritual.. you must all become bots!');

    const guild = zoopGuild;


    client.on('guildMemberUpdate', nicknameRitualListener);
    function nicknameRitualListener(oldMember: GuildMember, newMember: GuildMember) {
        const bots: GuildMember[] = _.filter(guild.members.array(), (member: GuildMember) => _.endsWith(member.nickname, 'bot'));
        const botNameCount = bots.length;

        if (_.endsWith(newMember.nickname, 'bot')) {
            giphySearch('robot').then(gifUrl => {
                mainChannel.send(`Excellent work, **${newMember.nickname}**. Welcome to the collective! \n${gifUrl}`);
            });
        }

        // Complete when we have 5 bots.
        if (botNameCount >= 5) {
            client.removeListener('guildMemberUpdate', nicknameRitualListener);
            mainChannel.send('Excellent work, bots! Now, celebrate! \nhttps://media.giphy.com/media/tczJoRU7XwBS8/giphy.gif');
            ritualCompletion.markComplete('nicknames');
        }
    }
}



/**
 * Banishment Ritual
 */
function banishmentRitual() {

}